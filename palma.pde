float dt;
PImage palma;
PImage klima;
PImage daljinac;
PImage termo;
float zeljT = 20;
float[] Tgraph;
int graphT;
boolean doneGraph;

// Moram imati klasu koja prestavlja paket upravljanja pod nazivom Actuation
class Actuation
{
  float P = 0;
}

// Moram imati klasu koja prestavlja merni rezultat pod nazivom Measurement
class Measurement
{
  float T = 0;
}

// Moja simulacija mora da nasledi klasu Simulation i da implementira
// metode Update, Measure, Control, Visualize
class MojaSimulacija extends Simulation
{
  float Ts = 20;
  float Pg = 0;
  float a = 60000;
  float tg = 40;
  float pgreska = 0;

  MojaSimulacija ()
  {
    // Podesim frekvenciju update-jta simulacije da bude 10 puta veca od
    // frame rate-jta (30 fps)
    SetUpdateMul(10);
    
    // Podesim periodu kontrolera tako da bude 5 puta manja od perioda simulacije
    SetControlMul(5);
    
    dt = GetUpdatePeriod();
  }

  // Napravim korak simulacije. Npr. uradim numericku integraciju diferencijalne jednacine.
  // Dato mi je i upravljanje koje je izgenerisao kontroler.
  void UpdateSobica()
  {
    Ts += 1/a * Pg * dt;// + 0.05*(sin(0.001*millis()) + 1);
  }
  
  void Update(Actuation input) 
  {
    Pg += 1/tg * (input.P - Pg) *dt;
    UpdateSobica();
    if(frameCount%20 == 0 && !doneGraph) {
      Tgraph[graphT] = Ts*5;
      graphT = (graphT+1)%width;
      doneGraph = true;
      println("Snaga Grejaca: " + Pg + "\tTemperatura:" + round(Ts*100)/100.0 + "\tZeljena Temperatura: " + zeljT);
    }
  }

  // Napravim merenja (merenja se uvek desava pre izvrsavanja kontrolera)
  Measurement Measure()
  {
    Measurement m = new Measurement();
    m.T = Ts;
    return m;
  }

  // Izracunam novo upravljanje na osnovu merenja
  Actuation Control(Measurement m)
  {
    Actuation o = new Actuation();
    float greska = zeljT - m.T;
    o.P = 1.129e4 * pgreska - 1.123e4 * greska;
    o.P = 10*o.P;
    //o.P = 5000*(greska-pgreska)/0.0033333f + 5000*0.15 *greska;
    //o.P = 1000;
    pgreska = greska;
    
    return o;
  }

  // Vizualizujem rezultat simulacije (frame rate je 30 fps)
  void Visualize()
  {
    background(255);
    image(palma, 0, 0);
    colorMode(HSB);
    tint(210 + Pg*0.001, abs(Pg)*20, 255);
    image(klima, 20, 170);
    noTint();
    colorMode(RGB);
    image(daljinac, 500, 100);
    image(termo, 340, 100);
    noStroke();
    fill(255, 0, 0);
    ellipse(360, 286, 30, 23);
    rect(355, 286, 10, -2.1*Ts - 96);
    stroke(0, 0, 255);
    line(310, -2.1*zeljT + 190, 410, -2.1*zeljT + 190);
    fill(255, 255, 0);
    ellipse(-320*cos(0.001*millis())+width/2, height - 320*sin(0.001*millis()), 50, 50);
    for (int i = 1; i < width; i++) {
      line(i-1, height/2 - Tgraph[i-1], i, height/2 - Tgraph[i]);
    }
  }
}

// Moram implementirati funkciju koja inicijalizuje simulaciju
// i u njoj postaviti globalnu promenljivu sim na instancu moje simulacije
void InitSimulation()
{
  surface.setSize(640, 360);
  stroke(255);
  
  sim = new MojaSimulacija();
}

static Simulation sim = null;

abstract class Simulation 
{ 
  abstract void Update(Actuation input);
  abstract Measurement Measure();
  abstract Actuation Control(Measurement m);
  abstract void Visualize();
  
  float GetUpdatePeriod()
  {
    return 1.0 / fps / updateMul;
  }
  
  float GetControlPeriod()
  {
    return GetUpdatePeriod() * measureMul;
  }
  
  void SetUpdateMul(int n)
  {
    updateMul = n;
  }
  
  void SetControlMul(int n)
  {
    measureMul = n;
  }
  
  // update period is 1.0 / fps / updateMul
  private int updateMul = 10;
  
  // Measurement period is update period * measureMul
  private int measureMul = 5;
  
  private Actuation currentOutput = new Actuation();
  
  private int fps = 30;
  
  private int measureCnt = 0;
} 

void setup() 
{
  InitSimulation();
  palma = loadImage("palma.jpg");
  palma.resize(200, 360);  
  klima = loadImage("klima.png");
  klima.resize(200, 100);
  daljinac = loadImage("daljinac.jpg");
  daljinac.resize(100, 200);
  termo = loadImage("termo.png");
  termo.resize(60, 200);
  frameRate(sim.fps);
  Tgraph = new float[width];
}

void draw() 
{   
  doneGraph = false;
  for (int i = 0; i < sim.updateMul; i++)
  {
    sim.Update(sim.currentOutput);
          
    if (sim.measureCnt % sim.measureMul == 0)
    {
      sim.currentOutput = sim.Control(sim.Measure());
      sim.measureCnt = 0;
    }
    
    sim.measureCnt++;
  }
  
  sim.Visualize();
} 

void keyPressed() {
  if (key == 'w') zeljT += 5;
  else if (key == 's') zeljT -=5;
}

void mousePressed() {
  fill(255, 0, 0);
  if (dist(mouseX, mouseY, 540, 197) < 20) {
    zeljT -= 5;
  }
  else if (dist(mouseX, mouseY, 560, 197) < 20) {
    zeljT += 5;
  }
}